package topics_parser;


import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.apache.lucene.benchmark.quality.QualityQuery;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;

/**
 * Represents a topic parser
 *
 * @version 1.00
 * @since 1.00
 */
public class ToucheTopicsReader {

    public static void main(String[] args) throws IOException {

        BufferedReader in = Files.newBufferedReader(Paths.get("topics/topics-task-1-only-titles.xml"));
        List<QualityQuery> queriesList = new ArrayList<>();
        XmlMapper mapper = new XmlMapper();
        Topic[] topics = mapper.readValue(in, Topic[].class);

        for (Topic t : topics) {
            String queryID = t.getNumber();
            HashMap<String, String> values = new HashMap<>();
            values.put(Searcher.TOPIC_FIELDS.TITLE, t.getTitle());
            queriesList.add(new QualityQuery(queryID, values));
        }

        QualityQuery[] queries = new QualityQuery[queriesList.size()];
        queriesList.toArray(queries);

    }


    /**
     * An object that represent a topic
     */
    static class Topic {
        /**
         * The topic number
         */
        private String number;

        /**
         * The title of the topic
         */
        private String title;



        /**
         * Returns the topic number
         *
         * @return the topic number
         */
        public String getNumber() {
            return number;
        }

        /**
         * Sets the topic number
         *
         * @param number the topic number
         */
        public void setNumber(String number) {
            this.number = number;
        }

        /**
         * Returns the topic title
         *
         * @return the topic title
         */
        public String getTitle() {
            return title;
        }

        /**
         * Sets the topic title
         *
         * @param title the topic title
         */
        public void setTitle(String title) {
            this.title = title;
        }


        }
}

