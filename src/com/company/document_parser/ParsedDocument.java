/*
 *  Copyright 2021 University of Padua, Italy
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package document_parser;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.lucene.document.Field;

/**
 * Represents a parsed document to be indexed.
 *
 * @version 1.00
 * @since 1.00
 */
public class ParsedDocument {

    /**
     * The names of the {@link Field}s within the index.
     * All fields of an entry.
     * @version 1.00
     * @since 1.00
     */
    public final static class FIELDS {
        public static final String ID = "id";
        public static final String CONCLUSION = "conclusion";
        public static final String PREMISES = "premises";
        public static final String TEXT = "text";
        public static final String STANCE = "stance";
        public static final String ANNOTATIONS = "annotations";
        public static final String CONTEXT = "context";
        public static final String ACQUISITION_TIME = "acquisitionTime";
        public static final String ASPECTS = "aspects";
        public static final String DISCUSSION_TITLE = "discussionTitle";
        public static final String MODE = "mode";
        public static final String SOURCE_DOMAIN = "sourceDomain";
        public static final String SOURCE_ID = "sourceId";
        public static final String SOURCE_TEXT = "sourceText";
        public static final String SOURCE_TEXT_CONCLUSION_START = "sourceTextConclusionStart";
        public static final String SOURCE_TEXT_CONCLUSION_END = "sourceTextConclusionEnd";
        public static final String SOURCE_TEXT_PREMISE_START = "sourceTextPremiseStart";
        public static final String SOURCE_TEXT_PREMISE_END = "sourceTextPremiseEnd";
        public static final String SOURCE_TITLE = "sourceTitle";
        public static final String SOURCE_URL = "sourceUrl";
        public static final String SENTENCES = "sentences";
        public static final String SENT_ID = "sent_id";
        public static final String SENT_TEXT = "sent_text";
    }


    /**
     * The id of the entry
     */
    private String id;

    /**
     * The premises of the entry
     */
    private String premises;

    /**
     * The annotations of the premises
     */
    private String annotations;

    /**
     * The stance of the premises
     */
    private String stance;
    /**
     * The text of the premises
     */
    private String text;

    /**
     * The conclusion of the entry
     */
    private String conclusion;

    /**
     * The context of the entry
     */
    private String context;
    /**
     * The acquisitionTime of the context field
     */
    private String acquisitionTime;
    /**
     * The aspects of the context field
     */
    private String aspects;
    /**
     * The discussionTitle of the context field
     */
    private String discussionTitle;
    /**
     * The mode of the context field
     */
    private String mode;
    /**
     * The sourceDomain of the context field
     */
    private String sourceDomain;
    /**
     * The sourceId of the context field
     */
    private String sourceId;
    /**
     * The sourceText of the context field
     */
    private String sourceText;
    /**
     * The sourceTextConclusionStart of the context field
     */
    private String sourceTextConclusionStart;
    /**
     * The sourceTextConclusionEnd of the context field
     */
    private String sourceTextConclusionEnd;
    /**
     * The sourceTextPremiseStart of the context field
     */
    private String sourceTextPremiseStart;
    /**
     * THe sourceTextPremiseEnd of the context field
     */
    private String sourceTextPremiseEnd;
    /**
     * The sourceTitle of the context field
     */
    private String sourceTitle;
    /**
     * The sourceUrl of the context field
     */
    private String sourceUrl;

    /**
     * The sentences of the entry
     */
    private ParsedSentences[] parsedSentences;



    /**
     * Creates a new parsed document
     *
     * @param id the unique document identifier.
     * @throws NullPointerException  if {@code id} and/or {@code body} are {@code null}.
     * @throws IllegalStateException if {@code id} and/or {@code body} are empty.
     */
    public ParsedDocument(final String id) {

        if (id == null) {
            throw new NullPointerException("Document identifier cannot be null.");
        }

        if (id.isEmpty()) {
            throw new IllegalStateException("Document identifier cannot be empty.");
        }
        this.id = id;
    }
    /**
     * Return the annotations of the premises
     * @return the annotations of the premises
     */
    public String getAnnotations(){return annotations;}
    /**
     * Sets the annotations of the premises
     * @param annotations of the premises
     */
    public void setAnnotations(String annotations){ this.annotations = annotations;}
    /**
     * Returns the stance of the premises
     * @return the stance of the premises
     */
    public String getStance(){ return stance;}
    /**
     * Sets the stance of the premises
     * @param stance of the premises
     */
    public void setStance(String stance){ this.stance = stance;}
    /**
     * Returns the text of the premises
     * @return the text of the premises
     */
    public String getText(){ return text;}
    /**
     * Sets the text of the premises
     * @param text of the premises
     */
    public void setText(String text){this.text = text;}

    /**
     * Returns the unique entry identifier.
     * @return the unique entry identifier.
     */
    public String getIdentifier() {
        return id;
    }

    /**
     * Sets the unique entry identifier.
     *
     * @param id the unique document identifier
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Returns the context of the entry
     * @return the context of the entry
     */
    public String getContext() {

        List<Pair<String,String>> pairs = new ArrayList<>();
        pairs.add(new ImmutablePair<>("acquisitionTime",this.acquisitionTime));
        pairs.add(new ImmutablePair<>("aspects", this.aspects));
        pairs.add(new ImmutablePair<>("discussionTitle",this.discussionTitle));
        pairs.add(new ImmutablePair<>("mode", this.mode));
        pairs.add(new ImmutablePair<>("sourceDomain",this.sourceDomain));
        pairs.add(new ImmutablePair<>("sourceId", this.sourceId));
        pairs.add(new ImmutablePair<>("sourceText",this.sourceText));
        pairs.add(new ImmutablePair<>("sourceTextConclusionStart", this.sourceTextConclusionStart));
        pairs.add(new ImmutablePair<>("sourceTextConclusionEnd",this.sourceTextConclusionEnd));
        pairs.add(new ImmutablePair<>("sourceTextPremiseStart", this.sourceTextPremiseStart));
        pairs.add(new ImmutablePair<>("sourceTextPremiseEnd",this.sourceTextPremiseEnd));
        pairs.add(new ImmutablePair<>("sourceTitle", this.sourceTitle));
        pairs.add(new ImmutablePair<>("sourceUrl",this.sourceUrl));
        return pairs.toString();
    }



    public void setAcquisitionTime(String acquisitionTime){ this.acquisitionTime = acquisitionTime;   }
    public void setAspects(String aspects){ this.aspects=aspects;}
    public void setDiscussionTitle(String discussionTitle){this.discussionTitle = discussionTitle;}
    public void setMode(String mode){ this.mode = mode;}
    public void setSourceDomain(String sourceDomain){this.sourceDomain = sourceDomain;}
    public void setSourceId(String sourceId){this.sourceId = sourceId;}
    public void setSourceText(String sourceText){this.sourceText = sourceText;}
    public void setSourceTextConclusionStart(String sourceTextConclusionStart){this.sourceTextConclusionStart = sourceTextConclusionStart;}
    public void setSourceTextConclusionEnd(String sourceTextConclusionEnd){this.sourceTextConclusionEnd = sourceTextConclusionEnd;}
    public void setSourceTextPremiseStart(String sourceTextPremiseStart){this.sourceTextPremiseStart = sourceTextPremiseStart;}
    public void setSourceTextPremiseEnd(String sourceTextConclusionEnd){this.sourceTextPremiseEnd = sourceTextConclusionEnd;}
    public void setSourceTitle(String sourceTitle){this.sourceTitle = sourceTitle;}
    public void setSourceUrl(String sourceUrl){this.sourceUrl = sourceUrl;}

    public String getAcquisitionTime(){return acquisitionTime;}
    public String getAspects(){return aspects;}
    public String getDiscussionTitle(){return discussionTitle;}
    public String getMode(){return mode;}
    public String getSourceDomain(){return sourceDomain;}
    public String getSourceId(){return sourceId;}
    public String getSourceText(){return sourceText;}
    public String getSourceTextConclusionStart(){return sourceTextConclusionStart;    }
    public String getSourceTextConclusionEnd(){return sourceTextConclusionEnd;}
    public String getSourceTextPremiseStart(){return sourceTextPremiseStart;}
    public String getSourceTextPremiseEnd(){return sourceTextPremiseEnd;}
    public String getSourceTitle(){return sourceTitle;}
    public String getSourceUrl(){return sourceUrl;}


    /**
     * Returns the premises of the document
     * @return the premises of the document
     */
    public String getPremises() {
        List<Pair<String,String>> pairs = new ArrayList<>();
        pairs.add(new ImmutablePair<>("text",this.text));
        pairs.add(new ImmutablePair<>("stance", this.stance));
        pairs.add(new ImmutablePair<>("annotations",this.annotations));
        return pairs.toString();
    }



    /**
     * Returns the conclusion of the discussion
     *
     * @return the conclusion of the discussion
     */
    public String getConclusion() {
        return conclusion;
    }

    /**
     * Sets the conclusion of the discussion
     *
     * @param conclusion the conclusion of the discussion
     */
    public void setConclusion(String conclusion) {
        this.conclusion = conclusion;
    }





    /**
     * Returns the sentences of the document
     *
     * @return the sentences of the document
     */
        public ParsedSentences[] getSentences() { return this.parsedSentences; }
       public void setParsedSentences(ParsedSentences[] parsedSentences){ this.parsedSentences = parsedSentences;  }


    @Override
    public final String toString() {
        ToStringBuilder tsb = new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append(FIELDS.ID, id)
                .append(FIELDS.CONCLUSION,getConclusion())
                .append(FIELDS.PREMISES, getPremises())
                .append(FIELDS.CONTEXT, getContext())
                .append(FIELDS.SENTENCES, getSentences());

        return tsb.toString();
    }

    @Override
    public final boolean equals(Object o) {
        return (this == o) || ((o instanceof ParsedDocument) && id.equals(((ParsedDocument) o).id));
    }

    @Override
    public final int hashCode() {
        return 37 * id.hashCode();
    }

    /**
     * Returns the body of the document
     *
     * @return the body of the document
     */
    public final String getFullBody() {
        return  id + "\n" +
                conclusion + "\n" +
                getPremises() + "\n" +
                getContext() + "\n" +
                Arrays.toString(getSentences());
    }

}
