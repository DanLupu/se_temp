package document_parser;

import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;



public class ParsedSentences {

    /**
     * The sent_id of the sentences
     */
    private String sent_id;

    /**
     * The sent_text of the sentences
     */
    private String sent_text;

    public String getSent_id() {
        return sent_id;
    }

    public String getSent_text() {
        return sent_text;
    }

    public void setSent_id(String sent_id) {
        this.sent_id = sent_id;
    }

    public void setSent_text(String sent_text) {
        this.sent_text = sent_text;
    }

    public String toString(){
        List<Pair<String,String>> pairs = new ArrayList<>();
        pairs.add(new ImmutablePair<>("sent_id",this.sent_id));
        pairs.add(new ImmutablePair<>("sent_text", this.sent_text));
        return pairs.toString();
    }
}
